voice {
    name = 'group5'
    locale {
        language = 'en'
        country = 'US'
    }
    gender = 'male'
    type = 'unit selection'
    description = 'fnord'
    license {
        name = 'Creative Commons Attribution-NoDerivs 3.0 Unported'
        shortName = 'CC-BY-ND'
        url = 'http://mary.dfki.de/download/by-nd-3.0.html'
    }
    samplingRate = 16000
    wantsToBeDefault = 30
}

