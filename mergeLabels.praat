tg = Read from file... voice16.TextGrid
ni_2 = Get number of intervals... 2
assert ni_2 == 1
it = Extract tier... 1
tor_all = Down to TableOfReal (any)
tor = Extract rows where label... "starts with" timit

for p to Object_'tor'.nrow
  prompt$ = extractWord$(Object_'tor'.row$[p], "") - ":"
  p_it = Read IntervalTier from Xwaves... build/lab/'prompt$'.lab
  p_tor = Down to TableOfReal (any)
  Formula... if col == 1 or col == 2 then self + Object_'tor'[p, "Start"] else self fi
  select tg
  for i to Object_'p_tor'.nrow
    nocheck Insert boundary... 2 Object_'p_tor'[i, "Start"]
    ni_2 = Get number of intervals... 2
    label$ = Object_'p_tor'.row$[i]
    Set interval text... 2 ni_2 'label$'
  endfor
  nocheck Insert boundary... 2 Object_'tor'[p, "End"]
  select p_tor
  plus p_it
  Remove
endfor

plus tor
plus tor_all
plus it
Remove
select tg
